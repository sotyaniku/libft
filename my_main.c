/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksarnyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/25 17:50:45 by ksarnyts          #+#    #+#             */
/*   Updated: 2017/02/13 15:59:24 by ksarnyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>
#include <ctype.h>

static void	ft_print(char *string)
{
	while (*string)
	{
		ft_putchar(*string);
		string++;
	}	
	ft_putchar('\n');
}

static void	ft_printshift(unsigned int shift, char *string)
{
	ft_putchar(*string + shift);
}

static char	ft_increment(char c)
{
	return (c + 1);
}

static char	ft_decrement(unsigned int i, char c)
{
	return (c - i);
}

static t_list *ft_f(t_list *elem)
{
	if (ft_strcmp(elem->content, "111") == 0)
	   elem->content = "aaa";
	return (elem);	
}	

static void ft_ff(t_list *elem)
{
	elem->content = "aaa";
	elem->next = NULL;
	printf("one iteration\n");
}	

int			main(void)
{
	/*ft_memset*/

	char str1_1[] = "qwert yuiop";
	char str1_2[] = "qwert yuiop";
	printf("\n***ft_memset:***\n\n");
	printf("string: %s\n", str1_1);
	printf("ft_memset 8 cells with '1': %s\n", ft_memset(str1_1, 49, 8));
	printf("memset 8 cells with '1': %s\n", memset(str1_2, 49, 8));

	/*ft_bzero*/

	int i2;
	int len2;

	i2 = 0;
	char str2_1[] = "qwert yuiop";
	char str2_2[] = "qwert yuiop";
	len2 = ft_strlen(str2_1);
	printf("\n***ft_bzero:***\n\n");
	printf("string: %s\n", str2_1);
	ft_bzero(str2_1, 8);
	bzero(str2_2, 8);
	printf("ft_bzero for 8 cells:\n");
	while (i2 < len2)
	{
		printf("memcell %d: ->%c<-\n", i2, str2_1[i2]);
		i2++;
	}
	i2 = 0;
	printf("bzero for 8 cells:\n");
	while (i2 < len2)
	{
		printf("ft_memcell %d: ->%c<-\n", i2, str2_2[i2]);
		i2++;
	}
	
	/*ft_memcpy*/

	char str3_1[] = "qwert yuiop";
	char str3_2[] = "00011000";
	char str3_3[] = "qwert yuiop";
	printf("\n***ft_memcpy:***\n\n");
	printf("string1: %s\n", str3_1);
	printf("string2: %s\n", str3_2);
	printf("ft_memcpy copy 5 elements: %s\n", ft_memcpy(str3_1, str3_2, 5));
	printf("memcpy copy 5 elements: %s\n", memcpy(str3_3, str3_2, 5));

	/*ft_memccpy*/

	char str4_1[] = "qwert yuiop";
	char str4_2[] = "00012000";
	char str4_3[] = "qwert yuiop";
	printf("\n***ft_memccpy:***\n\n");
	printf("string1: %s\n", str4_1);
	printf("string2: %s\n", str4_2);
	printf("ft_memccpy 8 elem till '2': %s\n", ft_memccpy(str4_1, str4_2, 50, 8));
	printf("memccpy copy 8 elem till '2': %s\n", memccpy(str4_3, str4_2, 50, 8));
	printf("string1 ft_memccpy: %s\n", str4_1);
	printf("string1 memcpy: %s\n", str4_3);

	/*ft_memmove*/

	char str5_1[] = "qwert yuiop";
	char str5_2[] = "qwert yuiop";
	char *str5_3 = &str5_1[6];
	char *str5_4 = &str5_2[6];
	printf("\n***ft_memmove:***\n\n");
	printf("string1: %s\n", str5_1);
	printf("string2: %s\n", str5_3);
	printf("ft_memmove 3 elements 2->1: %s\n", ft_memmove(str5_1, str5_3, 3));
	printf("memmove 3 elements 2->1: %s\n", memmove(str5_2, str5_4, 3));

	char str5_5[] = "qwert yuiop";
	char str5_6[] = "qwert yuiop";
	char *str5_7 = &str5_5[6];
	char *str5_8 = &str5_6[6];
	printf("string1: %s\n", str5_5);
	printf("string2: %s\n", str5_7);
	printf("ft_memmove 3 elements 1->2: %s\n", ft_memmove(str5_7, str5_5, 3));
	printf("memmove 3 elements 1->2: %s\n", memmove(str5_8, str5_6, 3));

	/*ft_memchr*/

	char *str6_1 = "qwertyqwett";
	printf("\n***ft_memchr:***\n\n");
	printf("string: %s\n", str6_1);
	printf("ft_memchr 10 cells find 1st t: %s\n", ft_memchr(str6_1, 116, 10));
	printf("memchr 10 cells find 1st t: %s\n", memchr(str6_1, 116, 10));
	printf("ft_memchr 10 cells find 1st a: %s\n", ft_memchr(str6_1, 97, 10));
	printf("memchr 10 cells find 1st a: %s\n", memchr(str6_1, 97, 10));

	/*ft_memcmp*/

	char str7_1[] = "qwert yuiop";
	char str7_2[] = "qwertaaaa";
	printf("\n***ft_memcmp:***\n\n");
	printf("string1: %s\n", str7_1);
	printf("string2: %s\n", str7_2);
	printf("ft_memcmp 4 cells: %d\n", ft_memcmp(str7_1, str7_2, 4));
	printf("memcmp 4 cells: %d\n", memcmp(str7_1, str7_2, 4));
	printf("ft_memcmp 7 cells: %d\n", ft_memcmp(str7_1, str7_2, 7));
	printf("memcmp 7 cells: %d\n", memcmp(str7_1, str7_2, 7));

	/*ft_strdup*/

	char *str9_1 = "qwertyqwett";
	char *str9_2;
	char *str9_3;
	printf("\n***ft_strdup:***\n\n");
	printf("string: %s\n", str9_1);
	str9_2 = ft_strdup(str9_1);
	str9_3 = strdup(str9_1);
	printf("ft_strdup: %s\n", str9_2);
	printf("strdup: %s\n", str9_3);
	printf("string: %p\n", &str9_1);
	printf("ft_strdup: %p\n", &str9_2);
	printf("strdup: %p\n", &str9_3);

	/*ft_striter*/

	char *str34 = "qwerty";
	printf("\n***ft_striter:***\n\n");
	ft_striter(str34, &ft_print);

	/*ft_striteri*/

	char *str35 = "abcdef";
	printf("\n***ft_striteri:***\n\n");
	printf("%s\n", str35);
	ft_striteri(str35, &ft_printshift);

	/*ft_strmap*/

	char *str36 = "abcdef";
	printf("\n\n***ft_strmap:***\n\n");
	printf("%s\n", str36);
	printf("%s\n", ft_strmap(str36, &ft_increment));

	/*ft_strmapi*/

	char *str37 = "abcdef";
	printf("\n\n***ft_strmapi:***\n\n");
	printf("%s\n", str37);
	printf("%s\n", ft_strmapi(str37, &ft_decrement));

	/*ft_strequ*/

	char *str38_1 = "qwe rt";
	char str38_2[10] = "qwe rt";
	printf("\n***ft_strequ:***\n\n");
	printf("%s\n", str38_1);
	printf("%s\n", str38_2);
	printf("function returns %d\n", ft_strequ(str38_1, str38_2));

	/*ft_strnequ*/

	char *str39_1 = "qwe rt";
	char str39_2[10] = "qwe rt";
	char *str39_3 = "qweasdfsdfbhn";
	char *str39_4 = "qwe";
	printf("\n***ft_strnequ:***\n\n");
	printf("str1: %s\n", str39_1);
	printf("str2: %s\n", str39_2);
	printf("str3: %s\n", str39_3);
	printf("str4: %s\n", str39_4);
	printf("should be equal: %d\n", ft_strnequ(str39_1, str39_2, 10));
	printf("should not be equal: %d\n", ft_strnequ(str39_2, str39_3, 4));
	printf("should be equal: %d\n", ft_strnequ(str39_3, str39_2, 3));
	printf("should not be equal: %d\n", ft_strnequ(str39_2, str39_4, 4));

	/*ft_strsub*/

	char *str40 = "Hello World!";
	printf("\n***ft_strsub:***\n\n");
	printf("string: %s\n", str40);
	printf("Copy 5 symbols, begin from 2:\n%s\n", ft_strsub(str40, 2, 5));
	printf("Copy 3 symbols, begin from 0:\n%s\n", ft_strsub(str40, 0, 3));
	printf("Copy 10 symbols, begin from 5:\n%s\n", ft_strsub(str40, 5, 10));

	/*ft_strjoin*/

	char *str41_1 = "Hello ";
	char *str41_2 = "world";
	printf("\n***ft_strjoin:***\n\n");
	printf("%s\n", str41_1);
	printf("%s\n", str41_2);
	printf("new string:\n%s\n", ft_strjoin(str41_1, str41_2));
	printf("new string:\n%s\n", ft_strjoin(str41_2, str41_1));

	/*ft_strtrim*/

	char str42_1[] = "     ";
	char str42_2[] = "   asdfg qwer   asd ert45 f   ";
	str42_2[3] = '\t';
	str42_2[29] = '\n';
	printf("\n***ft_strtrim:***\n\n");
	printf("begin->%s<-end\n", str42_2);
	printf("new string:\nbegin->%s<-end\n", ft_strtrim(str42_2));
	printf("begin->%s<-end\n", str42_1);
	printf("new string:\nbegin->%s<-end\n", ft_strtrim(str42_1));

	/*ft_strsplit*/

	char **array43;
	char *string43 = "***sdfd***sdf*45*f***rg***";
	printf("\n***ft_strsplit:***\n\n");
	printf("string: %s\n", string43);
	printf("words in string: %d\n", ft_words_count(string43, '*'));
	array43 = ft_strsplit(string43, '*');
	while (*array43)
	{
		printf("%s\n", *array43);
		array43++;
	}
	
	/*ft_itoa*/

	int number = -0;
	printf("\n***ft_itoa:***\n\n");
	printf("number: %d\n", number);
	printf("string: %s\n", ft_itoa(number));
	
	/*ft_lstmap*/

	t_list *list;
	t_list *new_list;
	printf("\n***ft_lstmap:***\n\n");
	ft_lstadd(&(list), ft_lstnew("111", 4));
	ft_lstadd(&(list), ft_lstnew("222", 4));
	ft_lstadd(&(list), ft_lstnew("333", 4));
	ft_lstadd(&(list), ft_lstnew("444", 4));
	ft_lstadd(&(list), ft_lstnew("555", 4));
	ft_lstadd(&(list), ft_lstnew("666", 4));
	new_list = ft_lstmap(list, &ft_f);
	while (new_list)
	{
		printf("%s\n", new_list->content);
		new_list = new_list->next;
	}

	/*ft_lstiter*/

	t_list *list_break;
	t_list *addr1;
	t_list *addr2;
	printf("\n***ft_lstiter:***\n\n");
	ft_lstadd(&(list_break), ft_lstnew("111", 4));
	ft_lstadd(&(list_break), ft_lstnew("222", 4));
	ft_lstadd(&(list_break), ft_lstnew("333", 4));
	addr1 = list_break;
	addr2 = list_break->next;
	printf("addr1: %p\naddr2: %p\n", addr1->next, addr2->next);
	ft_lstiter(list_break, &ft_ff);
	printf("addr1: %p\naddr2: %p\n", addr1->next, addr2->next);

	/*ft_lstaddlast*/

	t_list *list_2;
	printf("\n***ft_lstaddlast:***\n\n");
	ft_lstaddlast(&(list_2), ft_lstnew("111", 4));
	ft_lstaddlast(&(list_2), ft_lstnew("222", 4));
	ft_lstaddlast(&(list_2), ft_lstnew("333", 4));
	ft_lstaddlast(&(list_2), ft_lstnew("444", 4));
	ft_lstaddlast(&(list_2), ft_lstnew("555", 4));
	ft_lstaddlast(&(list_2), ft_lstnew("666", 4));
	printf("list size: %d\n", ft_lstsize(list_2));
	while (list_2)
	{
		printf("%s\n", list_2->content);
		list_2 = list_2->next;
	}
	
	/*ft_isspace*/

	char c;

	c = '\0';
	printf("\n***ft_isspace:***\n\n");
	while (c <= ' ')
	{
		printf("%d ", c);
		if (ft_isspace(c))
			printf("space symbol\n");
		else
			printf("not a space symbol\n");
		c++;
	}	
	return (0);
}
